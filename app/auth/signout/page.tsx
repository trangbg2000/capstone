"use client";
import { signOut } from "next-auth/react";
import React, { useEffect } from "react";

const SignOut = () => {
  useEffect(() => {
    signOut({
      callbackUrl: "/",
      redirect: true,
    });
  }, []);
};

export default SignOut;
