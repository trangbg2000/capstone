"use server";

import { generateRandomUsername } from "@/app/lib/generator";
import prisma from "@/app/lib/prisma";
import { validateEmail, validatePassword } from "@/app/lib/validate";
// @ts-ignore
import bcrypt from "bcryptjs";
interface resultProp {
  status: boolean;
  message: string;
}
export const signUp = async (email: string, password: string) => {
  const result: resultProp = { status: false, message: "" };
  if (!validateEmail(email)) {
    result.message = "invalid email";
    return result;
  }
  if (!validatePassword(password)) {
    result.message = "invalid password";
    return result;
  }
  const user = await prisma.user.findUnique({
    where: {
      email,
    },
  });

  console.log(user);
  if (user) {
    result.message = "user already exists";
    return result;
  }

  const passwordHash = bcrypt.hashSync(password, 10);
  const defaultName = generateRandomUsername();
  const defaultAvatar = "https://source.boringavatars.com/marble/120";

  await prisma.user.create({
    data: {
      email,
      passwordHash,
      name: defaultName,
      image: defaultAvatar,
    },
  });

  result.status = true;
  result.message = "created successfully";
  return result;
};
