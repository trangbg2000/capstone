"use client";
import SignOutBtn from "@/components/ui/sign-out-button";
import { useSession } from "next-auth/react";
import React from "react";

const Home = () => {
  const { data: session } = useSession();

  return (
    <div>
      {session?.user && `${session.user?.name}`}
      Home
      <SignOutBtn />
    </div>
  );
};

export default Home;
