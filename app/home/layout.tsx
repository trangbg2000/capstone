"use client";
import Header from "@/components/custom/header";
import MainNav from "@/components/custom/main-nav";
import { ThemeProvider } from "../components/theme-provider";
import { ThemeToggle } from "@/components/ui/theme-toggle";
export default function HomeLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <div>
      <div>
        <Header />
        <div>{children}</div>
      </div>
    </div>
  );
}
