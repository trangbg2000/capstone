export function generateRandomUsername() {
  const length = 8; // Length of the random username
  const characters =
    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"; // Characters to choose from
  let username = "user@";

  // Generate random characters to form the username
  for (let i = 0; i < length; i++) {
    username += characters.charAt(
      Math.floor(Math.random() * characters.length),
    );
  }

  return username;
}
