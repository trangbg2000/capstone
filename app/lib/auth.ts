import prisma from "@/app/lib/prisma";
import { Account, AuthOptions, Profile, Session, User } from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";
import GoogleProvider from "next-auth/providers/google";
import { JWT } from "next-auth/jwt";
import NextAuth from "next-auth/next";

import { PrismaAdapter } from "@auth/prisma-adapter";
// @ts-ignore
import bcrypt from "bcryptjs";
// @ts-ignore
import jwt from "jsonwebtoken";
import { prismaClient } from "@/prisma/client";
export const authOptions: AuthOptions = {
  // @ts-expect-error
  adapter: PrismaAdapter(prismaClient),
  providers: [
    GoogleProvider({
      clientId: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    }),
    CredentialsProvider({
      name: "credentials",

      credentials: {
        email: {
          label: "Email",
          type: "text",
          placeholder: "your@email.com",
        },
        password: {
          label: "Password",
          type: "password",
        },
      },
      authorize: async (credentials) => {
        if (!credentials) {
          return null;
        }

        const { email, password } = credentials;

        const user = await prisma.user.findUnique({
          where: {
            email,
          },
        });

        if (!user) {
          return null;
        }

        const userPassword = user.passwordHash;

        const isValidPassword = bcrypt.compareSync(password, userPassword);

        if (!isValidPassword) {
          return null;
        }

        return user;
      },
    }),
  ],
  pages: {
    signIn: "/auth/signin",
    signOut: "/auth/signout",
  },
  secret: process.env.NEXTAUTH_SECRET,
  jwt: {
    // async encode({ secret, token }) {
    //   if (!token) {
    //     throw new Error("No token to encode");
    //   }
    //   return jwt.sign(token, secret);
    // },
    // async decode({ secret, token }) {
    //   if (!token) {
    //     throw new Error("No token to decode");
    //   }
    //   const decodedToken = jwt.verify(token, secret);
    //   if (typeof decodedToken === "string") {
    //     return JSON.parse(decodedToken);
    //   } else {
    //     return decodedToken;
    //   }
    //},
  },
  session: {
    strategy: "jwt",
    maxAge: 30 * 24 * 60 * 60,
    updateAge: 24 * 60 * 60,
  },
  callbacks: {
    async jwt(params: {
      token: JWT;
      user?: User | undefined;
      account?: Account | null | undefined;
      profile?: Profile | undefined;
      isNewUser?: boolean | undefined;
    }) {
      if (params.user) {
        params.token.email = params.user.email;
      }
      if (params.account) {
        params.token = Object.assign({}, params.token, {
          access_token: params.account.access_token,
        });
      }

      return params.token;
    },
    async session(params: { session: Session; token: JWT; user: User }) {
      if (params.session.user) {
        this.session = Object.assign({}, this.session, {
          access_token: params.token.access_token,
        });
        params.session.user.email = params.token.email;
      }

      return params.session;
    },
  },
};
