/** @type {import('next').NextConfig} */
const nextConfig = {
  // async redirects() {
  //   return [
  //     {
  //       source: "/",
  //       destination: "/welcome",
  //       permanent: true,
  //     },
  //   ];
  // },
  rewrites: async () => [
    {
      source: "/welcome",
      destination: "/index.html",
    },
  ],
  images: {
    domains: ["lh3.googleusercontent.com"],
  },
  experimental: {},
};

export default nextConfig;
