"use client";

import { useSession } from "next-auth/react";
import Image from "next/image";
import React, { FC } from "react";

const UserButton = () => {
  const { data: session } = useSession();
  if (session?.user?.name != undefined) {
    const userName: string = session?.user?.name?.toString();
  }
  return (
    <div>
      <div className="flex  p-2 rounded-md">
        <Image
          className="rounded-full border border-gray-500 mr-2"
          src={
            session?.user?.image == null || session?.user?.image == undefined
              ? ""
              : session?.user?.image
          }
          alt="user avatar"
          width={40}
          height={40}
        />
        <div>
          <p>{session?.user?.name}</p>
          <p className="text-gray-400 text-sm">{session?.user?.email}</p>
        </div>
      </div>
    </div>
  );
};

export default UserButton;
