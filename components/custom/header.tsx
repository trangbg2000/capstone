"use client";

import React from "react";
import MainNav from "@/components/custom/main-nav";
import UserButton from "./user-button";
import Image from "next/image";
import { Input } from "../ui/input";
import { useSession } from "next-auth/react";
import { ThemeToggle } from "../ui/theme-toggle";
import { Button } from "../ui/button";
import { Plus } from "lucide-react";

const Header = () => {
  return (
    <div className="flex h-20 items-center px-4 justify-around border-b w-screen ">
      <div>
        <Image src={"/vercel.svg"} alt="logo" height={50} width={50} />
      </div>
      <div>
        <Input type="search" placeholder="Search" />
      </div>
      <MainNav className="mx-6" />
      <div className="flex items-center justify-center">
        <Button>
          <Plus />
          Create Topic
        </Button>
        <ThemeToggle />
        <UserButton />
      </div>
    </div>
  );
};

export default Header;
